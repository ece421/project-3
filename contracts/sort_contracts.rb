gem 'test-unit'
require 'test/unit'

module SortContracts

  include Test::Unit::Assertions

  def psort_preconditions(timeout, arr, &comparator)
    assert block_given?, "Comparator block please!"
    assert timeout.is_a?(Numeric), "Timeout was not an integer"
    assert arr.is_a?(Enumerable), "Array wasn't enumerable"
    assert comparator.respond_to?(:call), "Comparator isn't callable"
  end

  def merge_sort_preconditions(first, last, arr, &comparator)
    assert block_given?, "No comparator block = no sort for you"
    assert first.is_a?(Integer) && last.is_a?(Integer), "First and last weren't integers"
    assert first >= 0 && last >= 0, "First and last were negative"
    assert arr.is_a?(Enumerable), "Array wasn't an enumerable"
    assert comparator.respond_to?(:call), "Comparator isn't callable"
  end

  def merge_preconditions(objects, a_first, a_last, b_first, b_last, sorted_start, sorted_end, &comparator)
    assert block_given?, "What do you mean I need to provide a comparator?"
    assert objects.is_a?(Enumerable), "Objects aren't enumerable? That's no good"
    assert a_first.is_a?(Integer) && a_last.is_a?(Integer) && b_first.is_a?(Integer) && b_last.is_a?(Integer) &&
        sorted_start.is_a?(Integer) && sorted_end.is_a?(Integer), "Numbers weren't integers"
    assert a_first >= 0 && a_last >= 0 && b_first >= 0 && b_last >= 0 && sorted_start >= 0 &&
        sorted_end >=0, "Some numbers are negative"
    assert comparator.respond_to?(:call), "Who you gonna call? Not comparator that's for sure"
  end

  def special_merge_preconditions(objects, left, right, p, r, &comparator)
    assert block_given?, "Lost: One comparator block, reward 800 smarties"
    assert objects.is_a?(Enumerable), "Objects aren't enumerable"
    assert left.is_a?(Enumerable) && right.is_a?(Enumerable), "Left and right don't work"
    assert p.is_a?(Integer) && r.is_a?(Integer), "Ints aren't inty"
    assert comparator.respond_to?(:call), "Comparator isn't callable"
  end

  def bin_search_preconditions(a, b, mid, &comparator)
    assert block_given?, "If no comparator was given, then that means, oh no..."
    assert a.is_a?(Enumerable) && b.is_a?(Enumerable), "Enumerables they aren't"
    assert mid.is_a?(Integer), "Mid shuold probably be an integer"
    assert comparator.respond_to?(:call), "Comparator isn't callable"
  end

  def bin_search_postconditions(result, b)
    assert result == -1 || (result >= 0 && result < b.size), "Result be invalid size"
  end

end
