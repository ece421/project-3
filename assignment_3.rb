# Group 5 Assignment 3
# Chris Hut and Vincent Phung
require 'benchmark'
require './sort'


my_big_boy_array = []

puts "Generating large array..."
for could_have_probably_done_this_with_an_iterator in 1..20 do
  my_big_boy_array <<  rand(555).to_i
end


stuff = my_big_boy_array.dup
include Sort

def insertion_sort!(array)
  # From them wikis: http://en.wikibooks.org/wiki/Algorithm_Implementation/Sorting/Insertion_sort#Ruby
  for i in 1..(array.length() -1)
    value = array[i]
    j = i-1
    while j>= 0 and array[j] > value
      array[j+1] = array[j]
      j -= 1
    end
    array[j+1] = value
  end
end

# This is how we sort the array"
insertion_sort!(stuff)
puts "this is an unsorted array:"
puts "#{my_big_boy_array}"
puts "will now call the sorter"
psort(10, my_big_boy_array) { |i,j| i <=> j}
puts "this is my sorted array after the call to the parallel sort:"
puts "#{my_big_boy_array}"

if stuff != my_big_boy_array
  puts "Sorted arrays weren't equal, uh oh"
end

# Example of how to use the timer , timer takes time in seconds!
puts ""
puts "this shows the timer going off for a sort:"
psort(0.0000000000000000000000000000001, my_big_boy_array) { |i,j| i <=> j}
