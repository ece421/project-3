gem 'minitest'

require 'minitest/autorun'
require File.dirname(__FILE__) + '/../sort'

class SortTests < MiniTest::Unit::TestCase

  include Sort

  def setup
    @big = [4, 8, 15, 16, 23, 42, 108, 99, 9, 10]
    @same = Array.new(100, 5)
    @one = [4]
    @easy = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    @test_one = [2, 4, 6, 3, 1]
  end

  def test_easy
    arr = @test_one.dup
    sorted = psort(-1, arr) { |i,j| i <=> j}
    assert sorted.kind_of?(Array), "Result was not an array"
    assert sorted?(sorted), "Array was not sorted"
  end

  def test_success
    arr = @big.dup
    sorted = psort(100, arr) { |i, j| i <=> j }
    assert sorted.kind_of?(Array), "Result was not an array"
    assert sorted?(sorted), 'Array was not sorted'
  end

  def test_sort_empty
    # Checking if some exception is thrown
    arr = Array.new
    sorted = psort(100, arr) { |i, j| i <=> j }
    assert sorted.kind_of?(Array), "Result was not an array"
  end

  def test_already_sorted
    arr = @easy.dup
    sorted = psort(100, arr) { |i, j| i <=> j }
    assert sorted.kind_of?(Array), "Result was not an array"
    assert sorted?(sorted), 'Array was not sorted'
  end

  def test_reverse_order
    arr = @big.dup.reverse
    sorted = psort(100, arr) { |i, j| i <=> j }
    assert sorted.kind_of?(Array), "Result was not an array"
    assert sorted?(sorted), 'Array was not sorted'
  end

  def test_all_same
    arr = @same.dup
    sorted = psort(100, arr) { |i, j| i <=> j }
    assert sorted.kind_of?(Array), "Result was not an array"
    assert sorted?(sorted), 'Array was not sorted'
  end

  def test_big_boy_random
    arr = []
    for we_dont_need_no_iterators in 0..123 do
      arr << (1 + rand).to_i
    end

    sorted = psort(-1, arr) { |i, j| i <=> j}
    assert sorted.kind_of?(Array), "Result was not an array"
    assert sorted?(sorted), 'Array was not sorted'

  end

  def test_timeOut
    arr = @big.dup
    sorted = psort(0.00000000000001 , arr){ |i, j| i <=> j }
    assert sorted?(sorted) != false  , "how was the array sorted?"
  end

  def sorted? arr
    # Why thank you SO: http://stackoverflow.com/a/12901820/1684866
    arr.reduce{ |e1,e2| e1 <= e2 ? e2 : (return false) };
    arr.each_cons(2).all?{|i, j| i <= j}
    true
  end

end
