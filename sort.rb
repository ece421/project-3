# sort module that performs multi-threaded sort
require './contracts/sort_contracts'

module Sort

  include SortContracts

# the parallel sort method , if timeout seconds elapses program will stop sorting
  def psort(timeout, arr, &comparator)
    psort_preconditions(timeout, arr, &comparator)
    first = 0
    last = arr.size - 1
    if timeout > 0.0
      watchDoge = Thread.new do
        sleep timeout
        puts "The sort took way too long, much slow, very long, wow!"
        Thread.list.each do |thread|
          thread.kill
        end
      end
    end
	begin
    merge_sort first, last, arr, &comparator
	rescue ThreadError , NoMemoryError
		puts "Thread Error! Im gonna kill my threads."
		Thread.list.each do |thread|
			thread.kill
		end
	end
    return arr
  end

  # will sort threads using parallel merge sort algorithm
  def merge_sort(first, last, arr, &comparator)
    merge_sort_preconditions(first, last, arr, &comparator)
    mid = ((first + last)/2).floor()
  	if first < last
      leftThread = Thread.new do
        merge_sort(first, mid, arr, &comparator)
      end
      rightThread = Thread.new do 
        merge_sort(mid + 1, last, arr, &comparator)
      end
      leftThread.join
      rightThread.join
      merge(arr, first, mid, mid+1, last, first, last, &comparator)
    end
  end

  # the parallel merge method used in merge sort
  def merge(objects, a_first, a_last, b_first, b_last, sorted_start, sorted_end, &comparator)
    merge_preconditions(objects, a_first, a_last, b_first, b_last, sorted_start, sorted_end, &comparator)
    a, b = []
    a = objects[a_first..a_last]
    b = objects[b_first..b_last]
    if b.size > a.size
        opp_merge = Thread.new do
          merge(objects, b_first, b_last, a_first, a_last, sorted_start, sorted_end, &comparator)
        end
        opp_merge.join
    elsif sorted_end == 0
      objects[sorted_start] = a[0]
    elsif a.size == 1
      if comparator.call(a[0], b[0]) <= 0 
        objects[sorted_start] = a[0]
        objects[sorted_end] = b[0]
      else
        objects[sorted_start] = b[0]
        objects[sorted_end] = a[0]
      end
    else
      mid = ((a.size-1)/2).floor
      pos = bin_search(a, b, mid, &comparator)
      end_pos = mid + pos + 1
      # Since no position can be found we gonna merge normally
      if pos == -1
        special_merge(objects, a, b, sorted_start, sorted_end, &comparator)
        return
      end
      # do merge on first half
      left_collection = objects.dup
      left_thread = Thread.new do
        merge(left_collection, a_first, a_first+mid, b_first, b_first + pos, sorted_start, sorted_start+ end_pos, &comparator)
      end
      # do merge on second half
      right_collection = objects.dup
      right_thread = Thread.new do 
        merge(right_collection, a_first+mid+1, a_last, b_first+pos+1, b_last, sorted_start + end_pos+1, sorted_end, &comparator)
      end
      left_thread.join
      right_thread.join
      # put collections together
      (sorted_start..sorted_start+end_pos).each do |i|
        objects[i] = left_collection[i]
      end
      (sorted_start + end_pos + 1..sorted_end).each do |i|
        objects[i] = right_collection[i]
      end

    end
  end

  # basically the normal non parallel merge , only used when binary search can not find a valid index
  def special_merge(objects, left, right, p, r, &comparator)
  special_merge_preconditions(objects, left, right, p, r, &comparator)
	result = []
	while left.size > 0 && right.size > 0
		result << if comparator.call(left[0],right[0]) < 0
			left.shift
		else
			right.shift
		end
	end
 
	result.concat(left).concat(right)
	objects[p..r] = result

  end

  # binary search that satisfies condition:
  # find j such that b[j] <= a[mid] <= b[j+1]
  def bin_search(a, b, middle, &comparator)
    bin_search_preconditions(a, b, middle, &comparator)
    min = 0
    max = b.length - 2
    mid_element = a[middle]
    result = -1
    if b.length == 1
      max = min -1
    end
    while max > min
      mid = ((max + min)/2).floor

      if comparator.call(b[mid], mid_element) == 1
        max = mid -1
      elsif comparator.call(b[mid+1], mid_element) == -1
        min = mid + 1
      else
        result = mid
        break
      end
    end
    bin_search_postconditions(result, b)
    return result
  end



end
